package com.example.basalam.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.basalam.R
import com.example.basalam.databinding.ProductListItemBinding
import com.example.basalam.model.data.Product
import com.example.basalam.view.ProductItemClickListener
import com.example.basalam.view.holders.ProductViewHolder

class ProductListAdapter(val productList:ArrayList<Product>):RecyclerView.Adapter<ProductViewHolder>(),ProductItemClickListener {
    fun updateList(newProductList: ArrayList<Product>) {
        productList.clear()
        productList.addAll(newProductList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = DataBindingUtil.inflate<ProductListItemBinding>(
            inflater,
            R.layout.product_list_item,
            parent,
            false
        )
        return ProductViewHolder(view)
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.view.product = productList[position]
        holder.view.productItemClickListener = this
    }

    override fun onProductItemClick(v: View) {
        Log.v("clicked!","clicked")
    }

}
