package com.example.basalam.view.holders

import androidx.recyclerview.widget.RecyclerView
import com.example.basalam.databinding.ProductListItemBinding

class ProductViewHolder(var view: ProductListItemBinding) : RecyclerView.ViewHolder(view.root)