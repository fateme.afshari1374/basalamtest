package com.example.basalam.view

import android.view.View

interface ProductItemClickListener {
    fun onProductItemClick(v: View)
}