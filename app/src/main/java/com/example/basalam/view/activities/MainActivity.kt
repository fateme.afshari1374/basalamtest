package com.example.basalam.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.basalam.R
import com.example.basalam.model.data.Product
import com.example.basalam.model.di.DaggerProductsComponent
import com.example.basalam.utils.CustomApplicationClass
import com.example.basalam.view.adapters.ProductListAdapter
import com.example.basalam.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: ListViewModel

    @Inject
    lateinit var productListAdapter: ProductListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val appComponent = (applicationContext as CustomApplicationClass).appComponent
        DaggerProductsComponent.builder()
            .appComponent(appComponent)
            .build()
            .inject(this)
        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        viewModel.refresh()

        recyclerview_products.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = productListAdapter
        }

        refresh_layout.setOnRefreshListener {
            recyclerview_products.visibility = View.GONE
            listError.visibility = View.GONE
            progress_loading.visibility = View.VISIBLE
            viewModel.refresh()
            refresh_layout.isRefreshing = false
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.products.observe(this, Observer { products: List<Product> ->
            products?.let {
                recyclerview_products.visibility = View.VISIBLE
                productListAdapter.updateList(products as ArrayList<Product>)
            }
        })

        viewModel.loadError.observe(this, Observer {
            it?.let {
                listError.visibility = if (it) View.VISIBLE else View.GONE
            }
        })

        viewModel.loading.observe(this, Observer {
            it?.let {
                progress_loading.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    listError.visibility = View.GONE
                    recyclerview_products.visibility = View.GONE
                }
            }
        })
    }
}

