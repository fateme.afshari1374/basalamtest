package com.example.basalam.model.repository


import com.example.basalam.model.data.MainResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ProductApi {

    @Headers("Content-Type: application/json")
    @POST("/user")
    fun getProducts(@Body body: String): Single<MainResponse>
}