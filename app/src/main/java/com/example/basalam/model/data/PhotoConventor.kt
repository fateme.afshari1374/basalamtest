package com.example.basalam.model.data

import androidx.room.TypeConverter


class PhotoConventor {
    @TypeConverter
    fun toPhoto(value: String?): Photo {
        if (value == null || value.isEmpty()) {
            return Photo("")
        }
        return Photo(value)
    }

    @TypeConverter
    fun toString(value: Photo?): String {
        if (value == null) {
            return ""
        }
        return "${value?.url}"
    }
}