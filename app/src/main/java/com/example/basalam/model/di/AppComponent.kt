package com.example.basalam.model.di

import com.example.basalam.model.repository.ProductApiService
import dagger.Component
import javax.inject.Singleton

@Component
@Singleton
interface AppComponent {
    fun getProductService(): ProductApiService
}