package com.example.basalam.model.repository
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.basalam.model.data.Product

@Dao
interface ProductDao {
    @Insert
    suspend fun insert(vararg product: Product)
//: List<Long>
    @Query("SELECT * FROM product")
    suspend fun getLacalProducts(): List<Product>


    @Query("DELETE FROM product")
    suspend fun deleteAll()
}
