package com.example.basalam.model.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

data class Photo(
    @ColumnInfo(name = "url")
    @SerializedName("url")
    val url: String?
)
