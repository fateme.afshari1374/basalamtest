package com.example.basalam.model.di

import com.example.basalam.view.adapters.ProductListAdapter
import dagger.Module
import dagger.Provides

@Module
class ProductListAdapterModule {
    @Module
    companion object {
        @JvmStatic
        @Provides
        @PerActivity
        fun providePLA(): ProductListAdapter = ProductListAdapter(arrayListOf())
    }
}