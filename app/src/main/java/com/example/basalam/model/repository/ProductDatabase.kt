package com.example.basalam.model.repository
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.basalam.model.data.RatingConventor
import com.example.basalam.model.data.VendorConventor
import com.example.basalam.model.data.PhotoConventor
import com.example.basalam.model.data.Product

@Database(
    entities = arrayOf(
        Product::class
    ),
    version = 1
,exportSchema = false
)
@TypeConverters(PhotoConventor::class,
    VendorConventor::class,
    RatingConventor::class)
abstract class ProductDatabase : RoomDatabase() {
    abstract fun productDao(): ProductDao

    companion object {
        @Volatile
        private var instance: ProductDatabase? = null

        private val LOCK = Any()

        operator fun invoke(context: Context) = instance
            ?: synchronized(LOCK) {
            instance
                ?: buildDatabase(
                    context
                ).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            ProductDatabase::class.java,
            "product_database"
        ).build()
    }
}