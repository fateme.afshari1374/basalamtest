package com.example.basalam.model.di

import com.example.basalam.view.activities.MainActivity
import dagger.Component
import javax.inject.Singleton


@Component(dependencies = [AppComponent::class],
    modules = [ProductListAdapterModule::class , DBModule::class])
@PerActivity
interface ProductsComponent {
    fun inject(mainActivity: MainActivity)

    @Component.Builder
    interface Builder {
        fun appComponent(appComponent: AppComponent): Builder
        fun build(): ProductsComponent
    }
}