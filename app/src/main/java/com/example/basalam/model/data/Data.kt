package com.example.basalam.model.data

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Data(
    @ColumnInfo(name = "productSearch")
    @SerializedName("productSearch")
    val productSearch: ProductSearch?
)