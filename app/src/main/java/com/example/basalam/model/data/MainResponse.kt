package com.example.basalam.model.data

import androidx.room.ColumnInfo
import com.example.basalam.model.data.Data
import com.google.gson.annotations.SerializedName

data class MainResponse(
    @ColumnInfo(name = "data")
    @SerializedName("data")
    val data: Data?
)