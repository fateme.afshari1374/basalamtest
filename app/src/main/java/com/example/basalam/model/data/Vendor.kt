package com.example.basalam.model.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

data class Vendor(
    @ColumnInfo(name = "name")
    @SerializedName("name")
    val name: String?
)