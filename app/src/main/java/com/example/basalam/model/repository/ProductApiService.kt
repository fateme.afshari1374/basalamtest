package com.example.basalam.model.repository


import com.example.basalam.model.data.MainResponse
import io.reactivex.Single
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Inject

class ProductApiService @Inject constructor() {

    private  val BASE_URL: String = "https://api.basalam.com/api/"


       val api= Retrofit
            .Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(ProductApi::class.java)



    fun getProducts(): Single<MainResponse> {
        val paramObject = JSONObject()
        paramObject.put("query","query {productSearch(size: 20) {products {id name photo(size: LARGE) { url } vendor { name } weight price rating { rating count: signals } } } }")
       return api.getProducts(paramObject.toString())
    }


}