package com.example.basalam.model.data


import androidx.room.ColumnInfo
import com.example.basalam.model.data.Product
import com.google.gson.annotations.SerializedName

data class ProductSearch(
    @ColumnInfo(name = "products")
    @SerializedName("products")
    val products: List<Product>?
)