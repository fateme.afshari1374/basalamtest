package com.example.basalam.model.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters

import com.google.gson.annotations.SerializedName

@Entity
data class Product(
    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    val id: Int,

    @ColumnInfo(name = "name")
    @SerializedName("name")
    val name: String?,
    @TypeConverters(PhotoConventor::class)
    @ColumnInfo(name = "photo")
    @SerializedName("photo")
    val photo: Photo?,
    @TypeConverters(VendorConventor::class)
    @ColumnInfo(name = "vendor")
    @SerializedName("vendor")
    val vendor: Vendor?,

    @ColumnInfo(name = "weight")
    @SerializedName("weight")
    val weight: Int?,

    @ColumnInfo(name = "price")
    @SerializedName("price")
    val price: Int?,

    @TypeConverters(RatingConventor::class)
    @ColumnInfo(name = "rating")
    @SerializedName("rating")
    val rating: Rating?
)
