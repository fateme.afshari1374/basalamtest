package com.example.basalam.model.di

import android.content.Context
import com.example.basalam.model.repository.ProductDao
import com.example.basalam.model.repository.ProductDatabase
import dagger.Module
import dagger.Provides

@Module
class DBModule {

    @Module
    companion object {
        @JvmStatic
        @Provides
        @PerActivity
        fun provideDB(context: Context): ProductDatabase =
            ProductDatabase(context)
        @JvmStatic
        @Provides
        @PerActivity
        fun provideDao(db: ProductDatabase): ProductDao = db.productDao()

    }
}