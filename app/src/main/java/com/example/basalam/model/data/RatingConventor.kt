package com.example.basalam.model.data

import androidx.room.TypeConverter


class RatingConventor {
    @TypeConverter
    fun toRating (value:String?): Rating {
        if (value == null || value.isEmpty()) {
            return Rating(0.0, 0)
        }
        return Rating(
            value?.split(",")?.get(0)?.toDouble(), value?.split(",")?.get(1)?.toInt()
        )
    }
    @TypeConverter
    fun toString (value: Rating?):String {
        if (value == null ) {
            return "0"
        }
        return "${value?.rating}"+","+"${value?.count}"
    }
}