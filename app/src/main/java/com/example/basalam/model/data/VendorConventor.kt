package com.example.basalam.model.data

import androidx.room.TypeConverter

class VendorConventor {
    @TypeConverter
    fun toVendor(value: String?): Vendor {
        if (value == null || value.isEmpty()) {
            return Vendor("")
        }
        return Vendor(value)
    }

    @TypeConverter
    fun toString(value: Vendor?): String {
        if (value == null ) {
            return ""
        }
        return "${value?.name}"
    }
}