package com.example.basalam.model.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

data class Rating(
    @ColumnInfo(name = "rating")
    @SerializedName("rating")
    val rating: Double?,

    @ColumnInfo(name = "count")
    @SerializedName("count")
    val count: Int?
)