package com.example.basalam.utils

import android.app.Application
import com.example.basalam.model.di.AppComponent
import com.example.basalam.model.di.DaggerAppComponent

class CustomApplicationClass: Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.create()
    }
}