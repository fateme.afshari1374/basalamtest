package com.example.basalam.viewmodel


import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.basalam.model.data.MainResponse
import com.example.basalam.model.data.Product
import com.example.basalam.model.di.DaggerAppComponent
import com.example.basalam.model.repository.ProductApiService
import com.example.basalam.model.repository.ProductDao
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

class ListViewModel(application: Application) : BaseViewModel(application) {
    @Inject
    lateinit var productApiService: ProductApiService
    @Inject
    lateinit var productDao: ProductDao
    private val disposable = CompositeDisposable()

    val products = MutableLiveData<List<Product>>()
    val loadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    init {
        productApiService = DaggerAppComponent.create().getProductService()
    }
    fun refresh() {
        fetchFromRemote()
    }

    private fun fetchFromDatabase() {
        loading.value = true
        launch {
            val products = productDao.getLacalProducts()
            setProductsRetrived(products)
            Toast.makeText(getApplication(), "products retrived from database", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun fetchFromRemote() {
        loading.value = true
        disposable.add(
            productApiService.getProducts()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<MainResponse>() {
                    override fun onSuccess(response: MainResponse) {
                        response.data?.productSearch?.products?.let { setProductsRetrived(it) }
                        Toast.makeText(
                            getApplication(),
                            "products retrived from remote",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    override fun onError(error: Throwable) {
                        loading.value = false
                        loadError.value = true
                        error.printStackTrace()
                        Log.e("Product_API", error.message)
                    }
                })
        )
    }

    private fun setProductsRetrived(productList: List<Product>) {
        products.value = productList
        loadError.value = false
        loading.value = false
    }

    private fun storeProductsLocally(productList: List<Product>) {
        launch {
            productDao.deleteAll()
            productDao.insert(*productList.toTypedArray())
            setProductsRetrived(productList)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}